$(document).ready(function() {

$("#main").height($(window).height()-$("#nav").height());

$('select').material_select();


$('.datepicker').pickadate({
selectMonths: true, // Creates a dropdown to control month
selectYears: 15, // Creates a dropdown of 15 years to control year,
today: 'Today',
clear: 'Clear',
close: 'Ok',
closeOnSelect: false // Close upon selecting a date,
});



$('.timepicker').pickatime({
    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: false, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Clear', // text for clear-button
    canceltext: 'Cancel', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    ampmclickable: true, // make AM PM clickable
    aftershow: function(){} //Function for after opening timepicker
  });

$('select').material_select();
$('.collapsible').collapsible();

$("#main").height($(window).height()-$("#nav").height());

$(".collection-item").click(function(e) {
	e.preventDefault();
	$('#tables-collection .inner-form').hide();

	$(this).siblings('.inner-form').show();
});

$(".nav-wrapper").click(function(e) {
		e.preventDefault();
		$('#tables-collection .inner-form').hide();

		$(this).siblings('.inner-form').show();
	});

// Initialize select listeners

$(".collapsible-header").click(function(event) {
	// alert($(this).html());
	if(!$(this).hasClass('active')){
		query = "SELECT * FROM " + $(this).html()
		editor.setValue(query);
		$("#run").click();
	}
});

// Initialize insert listeners

$("#insert-student").click(function(event) {
	/* Act on the event */

	inputs = [];

	studno = $("#student-studno").val();
	studentname = $("#student-studentname").val();
	birthday = $("#student-birthday").val();
	degree = $("#student-degree").val();
	major = $("#student-major").val();
	unitsearned = $("#student-unitsearned").val();

	// inputs = [{"StudNo":studno},{"StudentName":studentname},{"Birthday":birthday},{"Degree":degree},{"Major":major},{"UnitsEarned":unitsearned}]

	cols = ["StudNo","StudentName","Birthday","Degree","Major","UnitsEarned"];
	vals = [$("#student-studno").val(), $("#student-studentname").val(),$("#student-birthday").val(),$("#student-degree").val(),$("#student-major").val(),$("#student-unitsearned").val()];

	cols_str = "";
	vals_str = "";
	for(var i in cols){

		if(vals[i]!=""){
			cols_str +=cols[i]+",";
			if(cols[i]!="UnitsEarned")
				vals_str +='"'+vals[i]+'"'+",";
			else
				vals_str +=vals[i]+",";
		}
	}

	cols_str = cols_str.slice(0, -1)
	vals_str = vals_str.slice(0, -1)

	query="INSERT INTO STUDENT("+cols_str+") VALUES("+vals_str+") "
	editor.setValue(query);
	$("#run").click();
});


$("#insert-studenthistory").click(function(event) {
	/* Act on the event */





	// inputs = [{"StudNo":studno},{"StudentName":studentname},{"Birthday":birthday},{"Degree":degree},{"Major":major},{"UnitsEarned":unitsearned}]

	cols = ["StudNo","Description","Action","DateFiled","DateResolved"];
	vals = [$("#sh-studentNumber").val(), $("#sh-Description").val(),$("#sh-Action").val(),$("#sh-datefiled").val(),$("#sh-dateresolved").val()];

	cols_str = "";
	vals_str = "";
	for(var i in cols){

		if(vals[i]!=""){
			cols_str +=cols[i]+",";
			if(cols[i]!="UnitsEarned")
				vals_str +='"'+vals[i]+'"'+",";
			else
				vals_str +=vals[i]+",";
		}
	}

	cols_str = cols_str.slice(0, -1)
	vals_str = vals_str.slice(0, -1)

	query="INSERT INTO STUDENTHISTORY("+cols_str+") VALUES("+vals_str+") "
	editor.setValue(query);
	$("#run").click();
});



$("#insert-course").click(function(event) {
	/* Act on the event */





	// inputs = [{"StudNo":studno},{"StudentName":studentname},{"Birthday":birthday},{"Degree":degree},{"Major":major},{"UnitsEarned":unitsearned}]

	cols = ["Cno","CTitle","CDesc","NoOfUnits","HasLab","SemOffered"];
	vals = [$("#courseNumber").val(), $("#courseTitle").val(),$("#courseDescription").val(),$("#courseNumberofUnits").val(),$("#course-haslab").val(),$("#course-semoffered").val()];

	cols_str = "";
	vals_str = "";
	for(var i in cols){

		if(vals[i]!=""){
			cols_str +=cols[i]+",";
			if(cols[i]!="NoOfUnits")
				vals_str +='"'+vals[i]+'"'+",";
			else
				vals_str +=vals[i]+",";
		}
	}

	cols_str = cols_str.slice(0, -1)
	vals_str = vals_str.slice(0, -1)

	query="INSERT INTO COURSE("+cols_str+") VALUES("+vals_str+") "
	editor.setValue(query);
	$("#run").click();
});


$("#insert-courseoffering").click(function(event) {
	/* Act on the event */





	// inputs = [{"StudNo":studno},{"StudentName":studentname},{"Birthday":birthday},{"Degree":degree},{"Major":major},{"UnitsEarned":unitsearned}]

	cols = ["Semester","AcadYear","CNo","Section","Time","MaxStud"];
	vals = [$("#CourseOfferingSemester").val(), $("#COAcadYear").val(),$("#COcourseNumber").val(),$("#COSection").val(),$("#COtime").val(),$("#COMaxStud").val()];

	cols_str = "";
	vals_str = "";
	for(var i in cols){

		if(vals[i]!=""){
			cols_str +=cols[i]+",";
			if(cols[i]!="MaxStud")
				vals_str +='"'+vals[i]+'"'+",";
			else
				vals_str +=vals[i]+",";
		}
	}

	cols_str = cols_str.slice(0, -1)
	vals_str = vals_str.slice(0, -1)

	query="INSERT INTO COURSEOFFERING("+cols_str+") VALUES("+vals_str+") "
	editor.setValue(query);
	$("#run").click();
});


$("#insert-studcourse").click(function(event) {
	/* Act on the event */





	// inputs = [{"StudNo":studno},{"StudentName":studentname},{"Birthday":birthday},{"Degree":degree},{"Major":major},{"UnitsEarned":unitsearned}]

	cols = ["StudNo","CNo","Semester","AcadYear"];
	vals = [$("#scoursestudentNumber").val(), $("#scoursecourseNumber").val(),$("#scourseSemester").val(),$("#scourseAcadYear").val()];

	cols_str = "";
	vals_str = "";
	for(var i in cols){

		if(vals[i]!=""){
			cols_str +=cols[i]+",";
			if(cols[i]!="MaxStud")
				vals_str +='"'+vals[i]+'"'+",";
			else
				vals_str +=vals[i]+",";
		}
	}

	cols_str = cols_str.slice(0, -1)
	vals_str = vals_str.slice(0, -1)

	query="INSERT INTO STUDCOURSE("+cols_str+") VALUES("+vals_str+") "
	editor.setValue(query);
	$("#run").click();
});



});


$("#run").click(function(){
	// data = {"query": $("#query").val()};
	// alert($("#query").val());
	// alert(editor.getValue());
	if(editor.getValue()!=""){

		$.ajax({
			url: 'http://127.0.0.1:5000/query',
			type: 'POST',
			dataType: 'json',
			data:{"query":editor.getValue()},

		    success: function(resp) {
		    	console.log(resp)

				htmlString = "";

				if(resp.valid_query=="True"){

					if(resp.check_type=="SELECT"){

						htmlString +="<table id='result' class='striped highlight'><thead><tr>";

						for(column in resp.columns)
							htmlString +="<th>"+resp.columns[column]+"</th>";

						htmlString+="</tr></thead>";

						for(row in resp.data){
							htmlString+="<tr>";
							for(col in resp.data[row]){
								htmlString +="<td>"+resp.data[row][col]+"</td>";
							}
							htmlString+="</tr>";
						}

						htmlString+="</table>";

					}
					else if(resp.check_type=="INSERT"){
						htmlString="<h4 class='grey-text text-lighten-1 '>Inserted "+resp.numrows+" row into the table.</h4>";
						console.log(resp.data)
					}
					else if(resp.check_type=="DELETE"){
						htmlString="<h4 class='grey-text text-lighten-1 '>Deleted "+resp.numrows+" rows from the table.</h4>";
						// htmlString = resp.data;
					}

	 				Materialize.toast('SQL is Valid', 4000, 'green');
	 				Materialize.toast('Query returned '+resp.numrows+' rows.', 4000, 'green') // 'rounded' is the class I'm applying to the toast
				}
				else{
					htmlString = resp.error;
	 				Materialize.toast('SQL is Invalid', 4000, 'red');

				}


				// $("#right-top").hide();
				// if(resp.check_type=="SELECT"){

				// 	$("#result").DataTable();
				// }
				// $("#right-top").html(htmlString).fadeIn('400', function() {
					$("#right-top").html(htmlString);
					// $("#right-top").hide();
				if(resp.check_type=="SELECT"){

					$("#result").DataTable({
						"lengthChange": false
					});
					// $("#result_filter").css("float","left");
					$("#result_filter").css("margin-top","10px");
					$("#result_filter").css("width","30%");
					$(".dataTables_filter").css("text-align","right");

					// $("thead").css("border-bottom","none");
					$("th").css("border-bottom","none");

	//   border:none!important;
	// }
					// $(".pagination>button").css("background-color", "#1565c0");
					// $(".pagination>button").css("border", "none");
					// $(".pagination>button").css("color", "white");
					// $(".pagination>button").css("margin", "3px");
					// $(".pagination>button").css("padding", "10px");

				}
					// $("#right-top").html(htmlString).fadeIn('400', function() {});



		}});
	}
	else{
	 	Materialize.toast('Please enter a query.', 4000, 'red');

		htmlString = "<div class='valign-wrapper center-align'><h3 class='grey-text text-lighten-1' style='width: 100%;'>Run a query or select a table.</h3></div>"
		$("#right-top").html(htmlString);
	}

});